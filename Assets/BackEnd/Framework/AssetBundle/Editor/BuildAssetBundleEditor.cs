﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Framework.Utils;

#if UNITY_EDITOR

using UnityEditor;

namespace Framework.AssetBundle.Editor
{
	/// <summary>
	/// Cette classe est une classe éditor rajoutant un onglet outils en haut à droite.
	/// Son but est de générer les asset bundles tous en récupérants les noms dans un .txt au fur et à mesure des ajouts.
	/// </summary>
	/// @Author: QMandou 

	public class BuildAssetBundleEditor
	{
		static string buildPersitentPath = $"{Application.dataPath}/../AssetBundles";

		static string[] notExportDirectoryName = { "shipbundle" };

		[MenuItem("Tools/Build All Asset Bundle (Android, IOS, Win64, Win32)", priority = 1)]
		static void BuildAllAssetBundle()
		{
			BuildTarget[] buildTargets = { BuildTarget.Android, BuildTarget.iOS, BuildTarget.StandaloneWindows64, BuildTarget.StandaloneWindows };
			BuildTargetAssetBundle(buildTargets);
			Debug.Log("Build All Asset Bundle with Sucess");
		}

		[MenuItem("Tools/Build Android Asset Bundle", priority = 1)]
		static void BuildAndroidAssetBundle()
		{
			BuildTarget[] buildTargets = { BuildTarget.Android };
			BuildTargetAssetBundle(buildTargets);
			Debug.Log("Build Android Bundle with Sucess");
		}

		[MenuItem("Tools/Build IOS Asset Bundle", priority = 1)]
		static void BuilIOSAssetBundle()
		{
			BuildTarget[] buildTargets = { BuildTarget.iOS };
			BuildTargetAssetBundle(buildTargets);
			Debug.Log("Build IOS Bundle with Sucess");
		}

		[MenuItem("Tools/Build Window 64b Asset Bundle", priority = 1)]
		static void BuilWindow64bAssetBundle()
		{
			BuildTarget[] buildTargets = { BuildTarget.StandaloneWindows64 };
			BuildTargetAssetBundle(buildTargets);
			Debug.Log("Build Window 64b Bundle with Sucess");
		}

		[MenuItem("Tools/Build Window 32b Asset Bundle", priority = 1)]
		static void BuilWindow32bAssetBundle()
		{
			BuildTarget[] buildTargets = { BuildTarget.StandaloneWindows };
			BuildTargetAssetBundle(buildTargets);
			Debug.Log("Build Window 32b Bundle with Sucess");
		}

		[MenuItem("Tools/Build Exportable Asset Bundle", priority = 2)]
		static void BuildExportAssetBundle()
		{
			BuildTarget[] buildTargets = { BuildTarget.Android, BuildTarget.iOS, BuildTarget.StandaloneWindows64 };
			string path = Path.Combine(Application.persistentDataPath, "AssetBundles");
			IOUtils.CreateDirectoryIfNotExist(path);

			for (int i = 0; i < buildTargets.Length; i++)
			{
				string targetPath = $"{path}/{buildTargets[i]}";

				IOUtils.CreateDirectoryIfNotExist(targetPath);
				BuildPipeline.BuildAssetBundles(targetPath, BuildAssetBundleOptions.None, buildTargets[i]);
			}

			ClearExportDirectory(path, notExportDirectoryName, buildTargets);

			foreach (BuildTarget target in buildTargets)
			{
				CreateRegisterFile(Path.Combine(path, $"{target}"), ".manifest", "register", $"{target}/");
			}
			Debug.Log("Export Asset Bundle with Sucess");
		}

		/// <summary>
		/// S'occupe de construire et de copier dans les streamings assets,
		/// les assets bundles pour les différentes plateforme séléctionnées.
		/// </summary>
		/// <param name="buildTargets"></param>
		static void BuildTargetAssetBundle(BuildTarget[] buildTargets)
		{
			IOUtils.CreateDirectoryIfNotExist(buildPersitentPath);
			IOUtils.CreateDirectoryIfNotExist(Application.streamingAssetsPath);

			for (int i = 0; i < buildTargets.Length; i++)
			{
				string targetPath = $"{buildPersitentPath}/{buildTargets[i]}";

				IOUtils.CreateDirectoryIfNotExist(targetPath);
				BuildPipeline.BuildAssetBundles(targetPath, BuildAssetBundleOptions.None, buildTargets[i]);

				IOUtils.CopyAllFolder(new DirectoryInfo(targetPath), new DirectoryInfo(Application.streamingAssetsPath));
			}

			MakeFileReference();
		}

		/// <summary>
		/// Cette fonction s'occupe de récupérer les noms des asset bundles 
		/// pour ensuite les implémentés dans un fichier qui serat lu et utilisé au runtime.
		/// </summary>
		static void MakeFileReference()
		{
			List<string> assetBundleNames = new List<string>();
			DirectoryInfo dirInfo = new DirectoryInfo(Application.streamingAssetsPath);

			foreach (DirectoryInfo dir in dirInfo.GetDirectories())
			{
				AddFileNameToList(dir, assetBundleNames);
			}

			string path = Path.Combine(Application.streamingAssetsPath, "AssetBundleNames.txt");
			dirInfo = new DirectoryInfo(path);
			FileStream stream = null;

			if (!dirInfo.Exists)
			{
				stream = File.Create(path);
			}
			else
			{
				stream = File.OpenWrite(path);
			}

			StreamWriter streamWriter = new StreamWriter(stream);

			foreach (string str in assetBundleNames)
			{
				streamWriter.WriteLine(str);
			}

			streamWriter.Close();
			stream.Close();
		}

		/// <summary>
		/// Récupère les noms de fichiers voulu et les ajoutes dans une liste.
		/// </summary>
		static void AddFileNameToList(DirectoryInfo dirInfo, List<string> list)
		{
			foreach (FileInfo fi in dirInfo.GetFiles())
			{
				// Les fichiers d'asset bundle n'ayant pas d'extension on repére ce qui n'on pas de point quelque chose pour les ajoutés à la liste. 
				int index = fi.Name.LastIndexOf('.');
				if (index < 0)
				{
					list.Add($"{dirInfo.Name}/{fi.Name}");
				}
			}
		}

		/// <summary>
		/// Permet de clean le dossier export de tous les assets bundle inutile
		/// </summary>
		/// <param name="path">Chemin reagarde</param>
		/// <param name="eraseNames">nom des bundles à détruire</param>
		/// <param name="targets"></param>
		static void ClearExportDirectory(string path, string[] eraseNames, BuildTarget[] targets)
		{
			for (int i = 0; i < targets.Length; i++)
			{
				string currentPath = Path.Combine(path, $"{targets[i]}");
				DirectoryInfo dirInfoPlatform = new DirectoryInfo(currentPath);

				// Delete les manifest des platformes (inutile et plutôt chiant dans notre cas)
				IOUtils.DeleteAllFiles(dirInfoPlatform);

				foreach (DirectoryInfo dirInfo in dirInfoPlatform.GetDirectories())
				{
					foreach (string s in eraseNames)
					{
						if (s == dirInfo.Name)
						{
							IOUtils.DeleteRecursivelyDirectory(dirInfo);
						}
					}
				}
			}
		}

		/// <summary>
		/// Recherche dans un dossier toute les entrées avec un .x donnée et les enregistres dans un fichier
		/// </summary>
		/// <param name="directoryPath">fichier dans lequel s'effectue la recherche</param>
		/// <param name="extension">type du fichier recherché</param>
		/// <param name="outputFileName">nom du fichier de sortie</param>
		static void CreateRegisterFile(string directoryPath, string extension, string outputFileName, string rootStart)
		{
			string outputFilePath = Path.Combine(directoryPath, outputFileName);
			IOUtils.CreateFileIfNotExist(outputFilePath);
			IOUtils.WriteAllText(outputFilePath, IOUtils.GetAllFilePathWithSameExtension(new DirectoryInfo(directoryPath), extension, rootStart).Concatenate());
		}
	}

}

#endif
