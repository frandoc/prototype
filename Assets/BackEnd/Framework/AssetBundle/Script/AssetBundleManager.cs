﻿
using System.IO;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Framework.Utils.DesignPattern;

namespace Framework.AssetBundleManager
{
	/// <summary>
	/// L'asset bundle manager est une classe destiné à s'occuper du chargement est déchargement des assets de manière automatique.
	/// L'utilisateur n'a qu'à demander un asset en particulier et si celui si existe le manager s'occupera de le charger. 
	/// Un asset non chargé renverra null, mais il est possible en utilisant LoadingAssetProgress d'avoir une valeur plus précise
	/// de la complétion du chargement.
	/// </summary>
	/// @Author : Q.Mandou

	public class AssetBundleManager : Singleton<AssetBundleManager>
	{
		// Container
		Dictionary<string, AssetBundle> assetBundleDictionnary;
		Dictionary<string, UnityEngine.Object> loadedObjectDictionnary;
		Dictionary<string, AssetBundleRequest> assetBundleRequestDictionnary;

		List<string> assetBundleNames;

		// Static Accessor
		static Dictionary<string, AssetBundle> AssetBundleDictionnary => Instance.assetBundleDictionnary;
		static Dictionary<string, UnityEngine.Object> LoadedObjectDictionnary => Instance.loadedObjectDictionnary;
		static Dictionary<string, AssetBundleRequest> AssetBundleRequestDictionnary => Instance.assetBundleRequestDictionnary;
		static List<string> AssetBundleNames => Instance.assetBundleNames;

		#region Initialize - Mono Start

		void Start()
		{
			assetBundleDictionnary = new Dictionary<string, AssetBundle>();
			loadedObjectDictionnary = new Dictionary<string, UnityEngine.Object>();
			assetBundleRequestDictionnary = new Dictionary<string, AssetBundleRequest>();

			Instance.StartCoroutine(GetAllBundleName());
		}

		#endregion

		#region AssetBundleRequest - Create loading request and release asset after use

		/// <summary>
		/// Return null si l'asset n'a pas finis de charger.
		/// </summary>
		public static T GetAsset<T>(string assetName) where T : UnityEngine.Object
		{
			assetName = assetName.ToLower();
			bool containAssetKey = false;
			string assetBundleName = GetAssetBundleName(assetName);

			if (assetBundleName == null)
			{
				return null;
			}

			containAssetKey = LoadedObjectDictionnary.ContainsKey(assetName);

			if (!containAssetKey && 
				AssetBundleDictionnary[assetBundleName] != null)
			{
				Instance.StartCoroutine(LoadAsset(assetBundleName, assetName));
			}
			else if (containAssetKey)
			{
				return LoadedObjectDictionnary[assetName] as T;
			}

			return null;
		}

		/// <summary>
		/// Permet de charger plusieurs asset en même temps. ne charge pas dans la liste les assets en chargement. Voir LoadingProgress()
		/// </summary>
		public static List<T> GetAssets<T>(List<string> assetNameList) where T : UnityEngine.Object
		{
			List<T> toReturn = new List<T>();

			foreach (string s in assetNameList)
			{
				T toAdd = GetAsset<T>(s);
				if (toAdd != null)
				{
					toReturn.Add(toAdd);
				}
			}

			return toReturn;
		}

		/// <summary>
		/// Permet de libérer la mémoire en libérant l'asset.
		/// </summary>
		public static void ReleaseAsset(string assetName)
		{
			assetName = assetName.ToLower();
			string assetBundleName = GetAssetBundleName(assetName);

			LoadedObjectDictionnary.Remove(assetName);

			if (assetBundleName != null)
			{
				AssetBundleDictionnary[assetBundleName].Unload(true);
				AssetBundleDictionnary.Remove(assetBundleName);

				if (Instance != null)
				{
					Instance.StartCoroutine(LoadAssetBundleAsync(assetBundleName));
				}
				else
				{
					Debug.LogError("Instance == null :: ReleaseAsset can't start coroutine.");
				}
			}
		}

		/// <summary>
		/// Libère tous les assets en même temps.
		/// </summary>
		public static void ReleaseAll()
		{
			List<string> keys = LoadedObjectDictionnary.Keys.ToList();
			foreach (string s in keys)
			{
				ReleaseAsset(s);
			}
		}

		/// <summary>
		/// Retourne un float normalisé du taux de complétion du chargement de l'asset. 
		/// </summary>
		public static float LoadingAssetProgress(string assetName)
		{
			assetName = assetName.ToLower();

			if (!AssetBundleRequestDictionnary.ContainsKey(assetName))
			{
				return -1.0f;
			}

			return AssetBundleRequestDictionnary[assetName].progress;
		}

		/// <summary>
		/// Retourne un float normalisé du taux de complétion du chargement de plusieur asset. 
		/// </summary>
		public static float LoadingAssetsProgress(List<string> assetsName)
		{
			float toReturn = 0.0f;

			foreach (string assetName in assetsName)
			{
				string parsedAssetName = assetName.ToLower();

				if (AssetBundleRequestDictionnary.ContainsKey(parsedAssetName))
				{
					toReturn += AssetBundleRequestDictionnary[assetName].progress;
				}
			}

			return toReturn / (float)assetsName.Count;
		}

		static IEnumerator LoadAssetBundleAsync(string assetBundleName)
		{
			AssetBundleDictionnary[assetBundleName] = null;
			using (UnityWebRequest www = UnityWebRequestAssetBundle.GetAssetBundle($"{Application.streamingAssetsPath}/{assetBundleName}"))
			{
				yield return www.SendWebRequest();

				if (www.isNetworkError || www.isHttpError)
				{
					Debug.LogError(www.error);
				}
				else
				{
					AssetBundleDictionnary[assetBundleName] = DownloadHandlerAssetBundle.GetContent(www);
				}
			}
		}

		/// <summary>
		/// Si l'asset bundle est bien charger lance le chargement de l'asset
		/// </summary>
		static IEnumerator LoadAsset(string assetBundleName, string assetName)
		{
			LoadedObjectDictionnary[assetName] = null;
			AssetBundleRequestDictionnary[assetName] = AssetBundleDictionnary[assetBundleName].LoadAssetAsync(assetName);

			while(!AssetBundleRequestDictionnary[assetName].isDone)
			{
				yield return null;
			}

			LoadedObjectDictionnary[assetName] = AssetBundleRequestDictionnary[assetName].asset;
			AssetBundleRequestDictionnary.Remove(assetName);
		}

		#endregion

		#region AssetBundlesNames - Parsing et récupération des noms de bundles

		static string GetAssetBundleName(string assetName)
		{
			foreach (string assetBundleName in AssetBundleNames)
			{
				if (AssetBundleDictionnary.ContainsKey(assetBundleName) &&
					AssetBundleDictionnary[assetBundleName] != null		&&
					ContainAssetName(AssetBundleDictionnary[assetBundleName].GetAllAssetNames(), assetName))
				{
					return assetBundleName;
				}
			}

			return null;
		}

		/// <summary>
		/// Permet de parser un chemin en nom d'asset pour savoir si il est bien présent dans la liste
		/// </summary>
		static bool ContainAssetName(string[] allName, string contain)
		{
			foreach (string s in allName)
			{
				int firstIndex = s.LastIndexOf('/') + 1;
				string result = s.Substring(firstIndex, s.LastIndexOf('.') - firstIndex);

				if (result == contain)
				{
					return true;
				}
			}
			return false;
		}

		/// <summary>
		/// Initialise la liste de nom de bundle en analysant les fichiers
		/// </summary>
		static IEnumerator GetAllBundleName()
		{
			Instance.assetBundleNames = new List<string>();
			string path = Path.Combine(Application.streamingAssetsPath, "AssetBundleNames.txt");

			UnityWebRequest request = UnityWebRequest.Get(path);

			yield return request.SendWebRequest();

			if (!string.IsNullOrEmpty(request.error))
			{
				Debug.LogError("Can't read file : " + request.error);
			}
			else
			{
				string toAdd = "";
				for (int i = 0; i < request.downloadHandler.text.Length; i++)
				{
					if (request.downloadHandler.text[i] == '\n')
					{
						AssetBundleNames.Add(toAdd);
						toAdd = "";
					}
					else
					{
						toAdd += request.downloadHandler.text[i];
					}
				}
			}

			foreach (string assetBundleName in AssetBundleNames)
			{
				yield return LoadAssetBundleAsync(assetBundleName);
			}
		}

		#endregion
	}
}