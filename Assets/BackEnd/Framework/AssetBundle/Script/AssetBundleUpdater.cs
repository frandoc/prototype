﻿
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.IO;
using Framework.Utils;

namespace Framework.AssetBundleManager
{
	public class AssetBundleUpdater : MonoBehaviour
	{
		public enum PlayPlatform
		{
			StandaloneWindows,
			StandaloneWindows64,
			Android,
			Invalid
		}

		public PlayPlatform GetPlatform()
		{
			switch (Application.platform)
			{
				case RuntimePlatform.WindowsEditor:
				case RuntimePlatform.WindowsPlayer:
					return IntPtr.Size > 4 ? PlayPlatform.StandaloneWindows64 : PlayPlatform.StandaloneWindows;
				case RuntimePlatform.Android:
					return PlayPlatform.Android;
			}

			return PlayPlatform.Invalid;
		}

		public class ManifestData
		{
			public string cachePath;
			public string bundlePath;
			public string bundleText;

			public ManifestData(string _cachePath, string _bundlePath)
			{
				cachePath = Path.Combine(_cachePath, _bundlePath);
				bundlePath = _bundlePath;
			}
		}

		public string ServerUrl;

		string bundleUrl;
		string cachePath;

		int manifestCount = 0;

		Dictionary<string, string> textDico;
		Dictionary<string, byte[]> byteDico;
		Dictionary<string, ManifestData> manifestDatas;

		// Start is called before the first frame update
		void Start()
		{
			if(ServerUrl.Length > 0)
			{
				bundleUrl = $"{ServerUrl}/AssetBundles/{GetPlatform()}";
				cachePath = $"{Application.persistentDataPath}/AssetBundles/{GetPlatform()}";

				StartCoroutine(CheckRegisterFile());
			}
		}

		IEnumerator RequestTextFromServer(string path, string key)
		{
			yield return RequestFromServer(path, h => textDico[key] = h.text);
		}

		IEnumerator RequestByteFromServer(string path, string key)
		{
			yield return RequestFromServer(path, h => byteDico[key] = h.data);
		}

		IEnumerator RequestFromServer(string path, Action<DownloadHandler> a)
		{
			using (UnityWebRequest www = UnityWebRequest.Get(path))
			{
				yield return www.SendWebRequest();

				if (www.isNetworkError || www.isHttpError)
				{
					Debug.LogError(www.error);
				}
				else
				{
					a(www.downloadHandler);
				}
			}
		}

		string GetTextFromDico(string key)
		{
			string toReturn = textDico[key];
			textDico[key] = null;
			return toReturn;
		}

		byte[] GetByteFromDico(string key)
		{
			byte[] toReturn = byteDico[key];
			byteDico[key] = null;
			return toReturn;
		}

		void InitContainer()
		{
			textDico = new Dictionary<string, string>();
			byteDico = new Dictionary<string, byte[]>();
			manifestDatas = new Dictionary<string, ManifestData>();
		}

		void ClearContainer()
		{
			textDico.Clear();
			byteDico.Clear();
			manifestDatas.Clear();
			textDico = null;
			byteDico = null;
			manifestDatas = null;
		}

		/// <summary>
		/// Récupère le fichier registre du serveur et recrée en local la même hiérarchie 
		/// que sur le serveur en fonction du fichier manifest.
		/// </summary>
		/// <returns></returns>
		IEnumerator CheckRegisterFile()
		{
			InitContainer();

			yield return RequestTextFromServer(Path.Combine(bundleUrl, "register"), "register");

			foreach (string bundlePath in textDico["register"].Split('\n'))
			{
				string path = Path.Combine(bundleUrl, bundlePath);
				string key = $"manifest{manifestCount}";
				manifestDatas[key] = new ManifestData(cachePath, bundlePath);

				yield return RequestTextFromServer(path, key);
				manifestCount++;
			}

			for (int i = 0; i < manifestCount; i++)
			{
				string key = $"manifest{i}";
				manifestDatas[key].bundleText = GetTextFromDico(key);

				if (File.Exists(manifestDatas[key].cachePath))
				{
					yield return RequestTextFromServer(manifestDatas[key].cachePath, key);

					if(!(GetHashFromManifest(GetTextFromDico(key)) == GetHashFromManifest(manifestDatas[key].bundleText)))
					{
						yield return ConstructAndDowloadCacheFile(manifestDatas[key]);
					}
				}
				else
				{
					yield return ConstructAndDowloadCacheFile(manifestDatas[key]);
				}
			}

			ClearContainer();
		}

		/// <summary>
		///  Construit si nécessaire, télécharge et enregistre les nouveaux fichiers.
		/// </summary>
		/// <param name="data"></param>
		/// <returns></returns>
		IEnumerator ConstructAndDowloadCacheFile(ManifestData data)
		{
			// Construction des dossiers
			IOUtils.CreateDirectoryIfNotExist(Path.Combine(Application.persistentDataPath,"AssetBundles"));
			IOUtils.CreateDirectoryIfNotExist(cachePath);

			string[] unconstructPath = data.bundlePath.Split('\\');
			for(int i = 0; i < unconstructPath.Length - 1; i++)
			{
				IOUtils.CreateDirectoryIfNotExist(Path.Combine(cachePath,unconstructPath[i]));
			}

			//Construction du fichier manifest
			IOUtils.CreateFileIfNotExist(data.cachePath);
			IOUtils.WriteAllText(data.cachePath, data.bundleText);

			// Construction et téléchargement du fichier bundle.
			string bundlePath = data.bundlePath.Substring(0, data.bundlePath.IndexOf(".manifest"));

			string key = "manifest";

			yield return RequestByteFromServer(Path.Combine(bundleUrl, bundlePath), key);

			string bundleCacheFilePath = Path.Combine(cachePath, bundlePath);
			IOUtils.CreateFileIfNotExist(bundleCacheFilePath);
			IOUtils.WriteAllBytes(bundleCacheFilePath, GetByteFromDico(key));
		}

		/// <summary>
		/// Récupère l'AssetFileHash du fichier manifest et le retourne sous forme de string
		/// </summary>
		/// <param name="text">texte à analyser</param>
		/// <returns></returns>
		string GetHashFromManifest(string text)
		{
			int hashPosition = text.IndexOf(" Hash:");
			string hash = "";

			for (int i = hashPosition + 6; i < text.Length; i++)
			{
				if (text[i] == '\n')
				{
					break;
				}

				hash += text[i];
			}

			return hash;
		}
	}

}