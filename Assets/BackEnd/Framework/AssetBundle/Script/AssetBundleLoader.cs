﻿
using UnityEngine;
using System.Collections;

namespace Framework.AssetBundleManager
{
	/// <summary>
	/// Abstract class permetant le chargement dynamique d'un asset à partir de son nom.
	/// Les fonctions surchargeable OnInitialize et OnEnd permettent d'éffectuer des opérations,
	/// avant et une fois le chargement terminé.
	/// </summary>
	/// @Author : Q.Mandou

	/// Class mère abstract générique
	public abstract class AssetBundleLoader<T> : MonoBehaviour where T : UnityEngine.Object
	{
		private System.Action abortEvent = null;

		[SerializeField] protected string assetName = "";
		[SerializeField] protected T assetLoaded = null;

		private void OnEnable()
		{
			StartCoroutine(InitializeEvent());
		}

		private void OnDisable()
		{
			Release();
		}

		private IEnumerator InitializeEvent()
		{
			OnInitialize();

			if(assetName != null && assetName.Length > 0)
			{
				while(assetLoaded == null)
				{
					LoadObject();
					yield return null;
				}

				OnEnd();
			}
			else
			{
				Debug.LogError($"You are trying to load invalid asset name {assetName}");
			}
		}

		protected virtual void LoadObject()
		{
			assetLoaded = AssetBundleManager.GetAsset<T>(assetName);
		}

		protected virtual void Release()
		{
			if(abortEvent != null)
				abortEvent();

			AssetBundleManager.ReleaseAsset(assetName);
			assetLoaded = null;
		}

		/// <summary>
		/// Fonction de fin se lançant lorsque l'asset est correctement chargé.
		/// Attention si la fonction ne se lance jamais c'est surrement que l'asset n'a pas pû correment se charger.
		/// </summary>
		protected abstract void OnEnd();

		/// <summary>
		/// Fonction d'initialisation se lançant avant la création de l'event.
		/// Utile pour modifier la valeur de 'assetName'
		/// </summary>
		protected abstract void OnInitialize();
	}

	/// <summary>
	///  Permet remplacer les valeurs d'un composant pars celui charger depuis les assets bundles
	///  ou de l'ajouter 
	/// </summary>
	/// <param name="IsAddedComponent">Si vrais ajoute un composant et lui fournis les valeurs de celui chargée</param>
	/// @Author : Q.Mandou
	public abstract class ComponentLoader<T> : AssetBundleLoader<T> where T : Component
	{
		[SerializeField] protected T targetedObject = null;
		[SerializeField] public bool IsAddedComponent = false;

		protected override void OnEnd()
		{
			if(IsAddedComponent)
			{
				targetedObject = gameObject.AddComponent<T>();
			}

			targetedObject = assetLoaded;
		}

		protected override void OnInitialize()
		{
			if(!IsAddedComponent)
				targetedObject = GetComponent<T>();
		}
	}

	/// <summary>
	///  Permet de charger puis d'instancier un prefabs dont le parent est le transform de l'objet.
	/// </summary>
	/// @Author : Q.Mandou
	public abstract class PrefabsLoader : AssetBundleLoader<GameObject>
	{
		protected GameObject instance = null;

		protected override void OnEnd()
		{
			instance = Instantiate(assetLoaded, transform);
		}
	}
}