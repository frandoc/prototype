﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace Framework.AI.BehaviorTree
{
	/// <summary>
	/// 
	/// </summary>
	/// @Author : Q.Mandou inspirée du code c++ de Colin Bruneau

	public class BehaviorTree
    {
        protected Behavior root;

		public Behavior Root => root;

		public void Update()
        {
            // Debug.Log("BehaviorTree Update");
            if (root != null) root.Update();
        }
    }
}