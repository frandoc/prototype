﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Framework.AI.BehaviorTree
{
	public class ExampleTreeMono : MonoBehaviour
	{
		ExampleBehaviorTree example;
		[SerializeField] string treeName = "";

		private void Start()
		{
			Entity entity = new Entity(treeName);
			example = new ExampleBehaviorTree(entity);
		}

		private void Update()
		{
			example.Update();
		}
	}

}