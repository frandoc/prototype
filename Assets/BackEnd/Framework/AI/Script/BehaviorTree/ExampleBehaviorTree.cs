﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace Framework.AI.BehaviorTree
{
	public class Entity
	{
		public Entity(string _name)
		{
			name = _name;
		}

		public bool Inside()
		{
			return inHouse;
		}

		public bool OutDoor()
		{
			return !inHouse;
		}

		public bool doorIsOutOfRange = false;
		public bool inHouse = false;
		public string name;
	}

	public class ExampleBehaviorTree : BehaviorTree
	{
		// Start is called before the first frame update
		public ExampleBehaviorTree(Entity entity)
        {
			root = new EntityLifeSelector(entity);
		}

		protected class EntityLifeSelector : Selector
		{
			public EntityLifeSelector(Entity entity)
			{
				AddChild(new LifeSelector(entity));
				AddChild(new ActionSeqence(entity));
			}
		}

		protected class ActionSeqence : Sequence
		{
			public ActionSeqence(Entity entity)
			{
				AddChild(new GoToDoorSequence(entity));
				AddChild(new DoorSequence(entity));
			}
		}
		
		protected class DoorSequence : Sequence
		{
			Entity entity;

			public DoorSequence(Entity _entity)
			{
				entity = _entity;

				AddChild(new Condition(() => !entity.doorIsOutOfRange));
				AddChild(new Action(OpenDoor));
				AddChild(new Timer(2.0f));
				AddChild(new Action(CloseDoor));
				AddChild(new Timer(2.0f));
			}

			public void OpenDoor()
			{
				entity.inHouse = !entity.inHouse;
				Debug.Log(entity.name + " a ouvert la porte est " + (entity.inHouse ? "rentre dans la maison." : "sort de la maison."));
			}

			public void CloseDoor()
			{
				Debug.Log(entity.name + " a fermé la porte.");
			}
		}

		protected class GoToDoorSequence : Sequence
		{
			Entity entity;

			public GoToDoorSequence(Entity _entity)
			{
				entity = _entity;

				Timer timeToWalkTheDoor = new Timer(2.0f);
				timeToWalkTheDoor.AddChild(new Action(() => WalkToDoor()));

				Timer timeToFindTheDoor = new Timer(2.0f);
				timeToFindTheDoor.AddChild(new Action(() => FindDoor()));

				AddChild(new Condition(() => entity.doorIsOutOfRange));
				AddChild(timeToFindTheDoor);
				AddChild(timeToWalkTheDoor);
				AddChild(new Timer(2.0f));
			}

			public void FindDoor()
			{
				Debug.Log(entity.name + " à trouvé la porte la porte.");
			}

			public void WalkToDoor()
			{
				Debug.Log(entity.name + " marche vers la porte.");
				entity.doorIsOutOfRange = false;
			}
		}

		protected class LifeSelector : Selector
		{
			public LifeSelector(Entity entity)
			{
				AddChild(new LifeInsideSequence(entity));
				AddChild(new LifeOutdoorSequence(entity));
			}
		}

		protected class LifeInsideSequence : Sequence
		{
			Entity entity;

			public LifeInsideSequence(Entity _entity)
			{
				entity = _entity;

				AddChild(new Condition(() => !entity.doorIsOutOfRange && entity.Inside()));
				AddChild(new Action(() => Life(entity)));
				AddChild(new Timer(2.0f));
			}

			public void Life(Entity entity)
			{
				Debug.Log(entity.name + " fais ça vie dans la maison");
				entity.doorIsOutOfRange = true;
			}

		}

		protected class LifeOutdoorSequence : Sequence
		{
			Entity entity;

			public LifeOutdoorSequence(Entity _entity)
			{
				entity = _entity;

				AddChild(new Condition(() => !entity.doorIsOutOfRange && entity.OutDoor()));
				AddChild(new Action(() =>  Life(entity)));
				AddChild(new Timer(2.0f));
			}

			public void Life(Entity entity)
			{
				Debug.Log(entity.name + " fais ça vie dans le jardin");
				entity.doorIsOutOfRange = true;
			}
		}
	}
}