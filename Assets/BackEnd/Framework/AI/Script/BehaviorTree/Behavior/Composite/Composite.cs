﻿
using System.Collections.Generic;

namespace Framework.AI.BehaviorTree
{
    public class Composite : Behavior
    {
        protected List<Behavior> behaviors;
        protected int behaviorIterator = 0;

        public Composite()
        {
            behaviors = new List<Behavior>();
        }

        // Behavior Methods

        public override void OnInitialize()
		{
			behaviorIterator = 0;
			Reset();
		}

        public override BehaviorStatus OnUpdate() { return BehaviorStatus.SUCESS; }

		public override void OnTerminate(BehaviorStatus _status) { }

		// Composite Methods

		protected void AddChild(Behavior child) { behaviors.Add(child); }
		protected void RemoveChild(Behavior child) { behaviors.Remove(child); }
		protected void RemoveAllChild(System.Predicate<Behavior> match) { behaviors.RemoveAll(match); }

        public Behavior GetChild(int index) { return index < behaviors.Count ? behaviors[index] : null; }
        public int GetChildCount() { return behaviors.Count; }
    }
}