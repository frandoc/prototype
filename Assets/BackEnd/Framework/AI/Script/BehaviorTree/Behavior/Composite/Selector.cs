﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace Framework.AI.BehaviorTree
{
    public class Selector : Composite
    {
        public override BehaviorStatus OnUpdate()
        {
            for (int i = behaviorIterator; i < behaviors.Count; i++)
			{
				BehaviorStatus status = behaviors[i].Update();
				behaviorIterator = i;

                if (status != BehaviorStatus.FAILURE)
                {
                    return status;
                }
            }

            return BehaviorStatus.FAILURE;
        }
	}
}