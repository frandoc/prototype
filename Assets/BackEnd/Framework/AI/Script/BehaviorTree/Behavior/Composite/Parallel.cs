﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Framework.AI.BehaviorTree
{
	public class Parallel : Composite
	{
		List<int> finishIndex;

		public override void OnInitialize()
		{
			base.OnInitialize();
			finishIndex = finishIndex ?? new List<int>();
		}

		public override BehaviorStatus OnUpdate()
		{
			for (int i = 0; i < behaviors.Count; i++)
			{
				if(!finishIndex.Contains(i))
				{
					BehaviorStatus status = behaviors[i].Update();

					if (status == BehaviorStatus.FAILURE || status == BehaviorStatus.SUCESS)
					{
						finishIndex.Add(i);
					}
				}
			}

			return BehaviorStatus.SUCESS;
		}

		public override void OnTerminate(BehaviorStatus _status)
		{
			finishIndex.Clear();
		}
	}

}