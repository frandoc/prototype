﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace Framework.AI.BehaviorTree
{
    public class Sequence : Composite
    {
		public override BehaviorStatus OnUpdate()
        {
            for (int i = behaviorIterator; i < behaviors.Count; i++)
			{
				BehaviorStatus status = behaviors[i].Update();
				behaviorIterator = i;

                if (status != BehaviorStatus.SUCESS)
                {
                    return status;
                }
            }

            return BehaviorStatus.SUCESS;
        }
    }
}