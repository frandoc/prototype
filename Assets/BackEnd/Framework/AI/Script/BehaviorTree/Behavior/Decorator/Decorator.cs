﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace Framework.AI.BehaviorTree
{
	public class Decorator : Behavior
	{
		protected Behavior decoredBehavior = null;

		public void AddChild(Behavior _behavior) => decoredBehavior = _behavior;

		public override void OnInitialize() { }
		public override void OnTerminate(BehaviorStatus _status) { }

		public override BehaviorStatus OnUpdate() { return BehaviorStatus.SUCESS; }

	}
}