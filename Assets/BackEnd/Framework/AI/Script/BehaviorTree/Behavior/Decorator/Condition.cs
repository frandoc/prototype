﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Framework.AI.BehaviorTree
{
	public class Condition : Decorator
	{
		protected Func<bool> condition;

		public Condition(Func<bool> _condition)
		{
			condition = _condition;
			base.OnInitialize();
		}

		public override BehaviorStatus OnUpdate()
		{
			if (condition())
			{
				if(decoredBehavior != null)
				{
					return decoredBehavior.Update();
				}

				return BehaviorStatus.SUCESS;
			}

			return BehaviorStatus.FAILURE;
		}
	}
}