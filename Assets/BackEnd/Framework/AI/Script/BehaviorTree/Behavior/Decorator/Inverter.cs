﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Framework.AI.BehaviorTree
{
	public class Inverter : Decorator
	{
		public override BehaviorStatus OnUpdate()
		{
			if(decoredBehavior != null)
			{
				BehaviorStatus status = decoredBehavior.Update();
				switch (status)
				{
					case BehaviorStatus.SUCESS:
						return BehaviorStatus.FAILURE;
					case BehaviorStatus.FAILURE:
						return BehaviorStatus.SUCESS;
					default:
						return status;
				}
			}

			return BehaviorStatus.SUCESS;
		}
	}

}