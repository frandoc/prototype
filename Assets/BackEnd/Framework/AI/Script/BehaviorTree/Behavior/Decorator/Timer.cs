﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Framework.AI.BehaviorTree
{
	public class Timer : Decorator
	{
		float duration = 0.0f;
		float elaspedTime = 0.0f;

		public Timer(float _duration) => duration = _duration;

		public override void OnInitialize()
		{
			elaspedTime = 0.0f;
		}

		public override BehaviorStatus OnUpdate()
		{
			if (elaspedTime >= duration)
			{
				if(decoredBehavior != null)
				{
					return decoredBehavior.Update();
				}

				return BehaviorStatus.SUCESS;
			}
			else
			{
				elaspedTime += Time.deltaTime;
				return BehaviorStatus.RUNNING;
			}
		}
	}

}