﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Framework.AI.BehaviorTree
{

	public class Action : Decorator
	{
		protected System.Action action;

		public Action(System.Action _action) => action = _action;

		public override BehaviorStatus OnUpdate()
		{
			action();

			if (decoredBehavior != null)
			{
				return decoredBehavior.Update();
			}

			return BehaviorStatus.SUCESS;
		}
	}

}