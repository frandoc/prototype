﻿
namespace Framework.AI.BehaviorTree
{
    public enum BehaviorStatus
    {
        INVALID,
        SUCESS,
        FAILURE,
        RUNNING,
        ABORTED
    };

    abstract public class Behavior
    {
        BehaviorStatus status;

        public abstract void OnInitialize();
        public abstract BehaviorStatus OnUpdate();
        public abstract void OnTerminate(BehaviorStatus _status);

        public Behavior()
        {
            status = BehaviorStatus.ABORTED;
        }

        public BehaviorStatus Update()
		{
			if (status != BehaviorStatus.RUNNING)
            {
                OnInitialize();
            }

            status = OnUpdate();

            if (status != BehaviorStatus.RUNNING)
            {
                OnTerminate(status);
            }

            return status;
        }

        public void Reset()
        {
            status = BehaviorStatus.INVALID;
		}

        public void Abort()
        {
            OnTerminate(BehaviorStatus.ABORTED);
            status = BehaviorStatus.ABORTED;
        }

        public bool IsTerminated()
        {
            return status == BehaviorStatus.SUCESS || status == BehaviorStatus.FAILURE;
        }

        public bool IsRunning()
        {
            return status == BehaviorStatus.RUNNING;
        }

        public BehaviorStatus GetStatus()
        {
            return status;
        }
    }
}