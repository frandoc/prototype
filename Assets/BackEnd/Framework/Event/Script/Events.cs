﻿using System;

namespace Framework.Events
{
	/// <summary>
	/// Les events permette le déclanchement d'une action de manière événementielle.
	/// L'émetteur va devoir s'enregistrer
	/// </summary>
	public static class Events
	{
		/// <summary>
		/// Utiliser pour enregistrer un nouveau type d'event.
		/// Celui-ci est lier à un objet appellé l'écouteur qui a pour but de déterminer lorsqu'un event est obsolète.
		/// </summary>
		public static void Register(this object _object, EventType _eventTypeEnum) => EventsWrapper.Register(_object, _eventTypeEnum);

		public static void UnRegister(this object _object, EventType _eventTypeEnum) => EventsWrapper.UnRegister(_eventTypeEnum);

		public static void Subscribe(this object _object, EventType _eventTypeEnum, Action a) => EventsWrapper.Subscribe(_eventTypeEnum, a);

		public static void UnSubscribe(this object _object, EventType _eventTypeEnum, Action a) => EventsWrapper.UnSubscribe(_eventTypeEnum, a);

		public static void Invoke(this object _object, EventType _eventTypeEnum) => EventsWrapper.Invoke(_eventTypeEnum);

		// TimeEvent
		public static void SubscribeTimedEvent(this object listener, float delay, Action a) => EventsWrapper.SubscribeTimedEvent(listener, a, delay);
	}

}
