﻿
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using Framework.Utils.DesignPattern;
using System.Collections;

namespace Framework.Events
{
	public class EventsWrapper : Singleton<EventsWrapper>
	{

		private Dictionary<EventType, EventStruct> eventWrapper;
		private TimedEventWrapper timedEventWrapper;

		protected new void Awake()
		{
			base.Awake();
			eventWrapper = new Dictionary<EventType, EventStruct>();
			timedEventWrapper = new TimedEventWrapper();

			// Use unity scene manager for know when old scene is unloaded and unload eventWrapper if necessary.
			SceneManager.sceneUnloaded += (Scene scene) => CleanEventWasNullListener();
		}

		private void Update()
		{
			timedEventWrapper.UpdateTimedEvent();
		}

		#region Event
		protected class EventStruct
		{
			public object listener;
			public List<Action> actionWrapper;

			public EventStruct(object _listener)
			{
				listener = _listener;
				actionWrapper = new List<Action>();
			}
		}

		/// <summary>
		/// Utiliser pour enregistrer un nouveau type d'event.
		/// Celui-ci est lier à un objet appellé l'écouteur qui a pour but de déterminer lorsqu'un event est obsolète.
		/// </summary>
		static internal void Register(object o, EventType eventType) => Instance.eventWrapper[eventType] = new EventStruct(o);

		/// <summary>
		/// Supprime un type d'event et l'action wrapper qui lui est lié. 
		/// </summary>
		static internal void UnRegister(EventType eventType) => Instance.eventWrapper.Remove(eventType);

		/// <summary>
		/// Ajoute une nouvelle action
		/// </summary>
		static internal void Subscribe(EventType eventType, Action action) => Instance.eventWrapper[eventType].actionWrapper.Add(action);

		/// <summary>
		/// Supprime une action lié à ce type d'event.
		/// </summary>
		static internal void UnSubscribe(EventType eventType, Action action) => Instance.eventWrapper[eventType].actionWrapper.Remove(action);

		/// <summary>
		/// Utiliser pour déclancher toute les actions enregistré sur ce type.
		/// </summary>
		static internal void Invoke(EventType eventType)
		{
			if(Instance.eventWrapper[eventType].listener != null)
			{
				Instance.eventWrapper[eventType].actionWrapper.ForEach(a => a());
			}
			else
			{
				Debug.LogError($"Listener of {eventType} event is null ref");
			}
		}

		/// <summary>
		/// Nettoie tout les events dont les écouteurs sont null ref.
		/// Appeller de manière automatique après chaque déchargement de scène.
		/// </summary>
		static internal void CleanEventWasNullListener()
		{
			List<EventType> toErase = new List<EventType>();

			Instance.eventWrapper.ToList().ForEach(keyPairValue => 
			{ 
				if (keyPairValue.Value.listener == null)
				{
					toErase.Add(keyPairValue.Key);
				}
			});

			toErase.ForEach(eventType => Instance.eventWrapper.Remove(eventType));
		}

		#endregion

		#region Timed Event
		protected class TimedEventWrapper
		{
			public struct TimedEventStruct
			{
				public object listener;
				public Action action;
				public float timer;

				public TimedEventStruct(object _listener, Action _action, float _timer)
				{
					listener = _listener;
					action = _action;
					timer = _timer;
				}
			}

			public List<TimedEventStruct> timedEventWrapper;

			public TimedEventWrapper()
			{
				timedEventWrapper = new List<TimedEventStruct>();
			}

			public void UpdateTimedEvent()
			{

				for (int i = 0; i < timedEventWrapper.Count; i++)
				{
					if (timedEventWrapper[i].listener == null)
					{
						timedEventWrapper.RemoveAt(i);
						i--;
					}
					else if (Time.time >= timedEventWrapper[i].timer)
					{
						timedEventWrapper[i].action();
						timedEventWrapper.RemoveAt(i);
						i--;
					}
				}
			}

			public void AddTimedEvent(object listener, Action action, float delay) => timedEventWrapper.Add(new TimedEventStruct(listener, action, Time.time + delay));

		}

		static internal void SubscribeTimedEvent(object listener, Action action, float delay) => Instance.timedEventWrapper.AddTimedEvent(listener, action, delay);

		#endregion
	}
}

