﻿
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Framework.StateManager
{
	public interface IState
	{
		void OnEnter();
		void OnUpdate();
		void OnExit();
	}

	public struct StateSwap
	{
		public delegate bool Condition();

		State state;
		Condition condition;

		public StateSwap(State _state, Condition _condition)
		{
			state = _state;
			condition = _condition;
		}

		void LookTransition()
		{
			if (condition())
			{
				StateManager.Instance.currentState = state;
				state.OnEnter();
			}
		}
	}

	/// <summary>
	///  Class inherited by any state and substate 
	/// </summary>
	public abstract class State : IState
	{
		string name; 
		Action action;
		List<StateSwap> linkedState;
		State subState;

		public State(string _name, Action _action, State _subState = null)
		{
			name = _name;
			action = _action;
			subState = _subState;
			linkedState = new List<StateSwap>();
		}

		/// <summary>
		/// Throw exception to remenber not filled inherited function in any inherited State.
		/// DONT CALL this, it called for init after transition by state manager.
		/// Use strategy to securise and keep extern use by statemanager for "OnEnter" function. 
		/// </summary>
		public virtual void OnEnter()
		{
			throw new System.NotImplementedException();
		}

		public virtual void OnUpdate()
		{
			throw new System.NotImplementedException();
		}

		public virtual void OnExit()
		{
			throw new System.NotImplementedException();
		}
	}
}