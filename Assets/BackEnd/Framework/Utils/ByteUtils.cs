﻿
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System;

namespace Framework.Utils
{
	public class ByteUtils
	{
		public static byte[] ToByte(object o)
		{
			if(o == null)
			{
				return null;
			}

			using (MemoryStream ms = new MemoryStream())
			{
				IFormatter bf = new BinaryFormatter();
				bf.Serialize(ms, o);
				return ms.ToArray();
			}
		}

		public static T FromByte<T>(byte[] bytes)
		{
			using (MemoryStream ms = new MemoryStream(bytes))
			{
				IFormatter bf = new BinaryFormatter();
				return (T)bf.Deserialize(ms);
			}
		}

		public static T FromByte<T>(byte[] bytes, int sizeofT, ref int offest)
		{
			return FromByte<T>(GetPartOfArray(bytes, sizeofT, ref offest));
		}

		public static T[] FillArrayFromByte<T>(byte[] bytes, int lenght, int sizeofT, ref int offset)
		{
			T[] toReturn = new T[lenght];
			for (int i = 0; i < lenght; i++)
			{
				toReturn[i] = FromByte<T>(bytes, sizeofT, ref offset);
			}
			return toReturn;
		}

		public static int[] GetSizeArray<T>(T[] datas)
		{
			int[] toReturn = new int[datas.Length];
			for(int i = 0; i < datas.Length; i++)
			{
				toReturn[i] = ToByte(datas[i]).Length;
			}
			return toReturn;
		}

		public static byte[] GetPartOfArray(byte[] bytes, int lenght, ref int offset)
		{
			byte[] toReturn = new byte[lenght];
			Array.Copy(bytes, offset, toReturn, 0, lenght);
			offset += lenght;
			return toReturn;
		}
	}
}
