﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Framework.Utils.DesignPattern
{

	/// <summary>
	/// Singleton pattern can be used to make global function or global data container used by lot of features.
	/// </summary>
	/// <typeparam name="T"></typeparam>	
	[Serializable]
	public class Singleton<T> : MonoBehaviour where T : Singleton<T>
	{
		static Singleton<T> singleton;

		static public T Instance
		{
			get => singleton as T;
			protected set => singleton = value;
		}

		protected void Awake()
		{
			if(singleton == null)
			{
				singleton = this;

				// if buildIndex == -1 the gameobject is already 'DontDestroyOnLoad'
				if (gameObject.scene.buildIndex != -1)
				{
					DontDestroyOnLoad(gameObject);
				}
			}
			else
			{
				Destroy(gameObject);
			}
		}
	}
}
