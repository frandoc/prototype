﻿using System;

namespace Framework.Utils
{
	public static class DataUtils
	{
		public static void MergeArray<T>(ref T[] dest, T[] src)
		{
			int originalLenght = dest.Length;
			Array.Resize<T>(ref dest, originalLenght + src.Length);
			Array.Copy(src, 0, dest, originalLenght, src.Length);
		}
	}
}
