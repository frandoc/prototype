﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Framework.Utils
{
	public class MapValue<T>
	{
		public int Width { get; private set; }
		public int Height { get; private set; }

		public int Size => Width * Height;

		T[] container;

		public MapValue(int width, int height)
		{
			Width = width;
			Height = height;
			container = new T[Size];
		}

		int GetIndex(Vector2Int position) => position.x + position.y * Width;

		public T this[Vector2Int position]
		{
			get => container[GetIndex(position)];
			set => container[GetIndex(position)] = value;
		}

		public void Spread(int width, int height)
		{
			int oldWidth = Width;
			int oldHeight = Height;
			T[] oldContainer = container;

			Width = width;
			Height = height;
			container = new T[Size];

			for (int x = 0; x < oldWidth && x < Width; x++)
			{
				for (int y = 0; y < oldHeight && y < Height; y++)
				{
					Vector2Int position = new Vector2Int(x, y);
					container[GetIndex(position)] = oldContainer[GetIndex(position)];
				}
			}
		}

	}
}
