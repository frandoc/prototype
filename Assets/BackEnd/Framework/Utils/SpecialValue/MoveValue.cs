﻿using System;
using UnityEngine;

namespace Framework.Utils
{
	[Serializable]
	public class MoveValue
	{
		float deltaTime => Time.deltaTime;

		public float MaxSpeed { get; set; }

		public float Acceleration { get; set; }

		public TripValue TripValue { get; set; }

		public MoveValue(float maxSpeed, float acceleration, TripValue tripValue)
		{
			MaxSpeed = maxSpeed;
			Acceleration = acceleration;
			TripValue = tripValue;
		}

		public void UpdateSpeed()
		{
			float nextAccelarationValue = TripValue.Speed + Acceleration * deltaTime;

			if (nextAccelarationValue >= MaxSpeed)
			{
				nextAccelarationValue = MaxSpeed;
			}
			else if(nextAccelarationValue <= 0.0f)
			{
				nextAccelarationValue = 0.0f;
			}

			TripValue.Speed = nextAccelarationValue;
		}

		public override string ToString() => $"MaxSpeed : {MaxSpeed}, Acceleration : {Acceleration}, TripValue => {TripValue}";
	}
}