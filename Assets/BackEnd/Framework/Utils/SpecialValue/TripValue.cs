﻿using System;

namespace Framework.Utils
{
	[Serializable]
	public class TripValue
	{
		const int timeIndex = 0;
		const int distanceIndex = 1;
		const int speedIndex = 2;

		public const int tripValueCount = 3;

		float[] tripValue;
		bool[] tripValueHasChanged;

		public TripValue()
		{
			tripValue = new float[] { 0.0f, 0.0f, 0.0f };
			tripValueHasChanged = new bool[] { false, false, false };
		}

		public TripValue(float _time = 0.0f, float _distance = 0.0f, float _speed = 0.0f)
		{
			tripValue = new float[] { _time, _distance, _speed };
			tripValueHasChanged = new bool[] { false, false, false };
		}

		public float Time
		{
			get => tripValue[timeIndex];
			set => SetValue(timeIndex, value);
		}

		public float Distance
		{
			get => tripValue[distanceIndex];
			set => SetValue(distanceIndex, value);
		}

		public float Speed
		{
			get => tripValue[speedIndex];
			set => SetValue(speedIndex, value);
		}

		void SetValue(int index, float value)
		{
			tripValue[index] = value;
			tripValueHasChanged[index] = true;
			CheckValueHasChanged();
		}

		/// <summary>
		/// Time = Distance / Speed
		/// </summary>
		void UpdateTime()
		{
			if (!tripValueHasChanged[timeIndex] && Distance != 0.0f && Speed != 0.0f)
				Time = Distance / Speed;
		}

		/// <summary>
		/// Distance = Speed x Time
		/// </summary>
		void UpdateDistance()
		{
			if (!tripValueHasChanged[distanceIndex] && Speed != 0.0f && Time != 0.0f)
			{
				Distance = Speed * Time;
			}
		}

		/// <summary>
		/// Speed = Distance / Time
		/// </summary>
		void UpdateSpeed()
		{
			if (!tripValueHasChanged[speedIndex] && Distance != 0.0f && Time != 0.0f)
			{
				Speed = Distance / Time;
			}
		}

		void CheckValueHasChanged()
		{
			for (int i = 0; i < tripValueCount; i++)
			{
				if (tripValueHasChanged[i])
				{
					UpdateOtherValue(i);
					tripValueHasChanged[i] = false;
				}
			}
		}

		void UpdateOtherValue(int index)
		{
			switch (index)
			{
				case timeIndex:
					UpdateDistance();
					UpdateSpeed();
					break;
				case distanceIndex:
					UpdateTime();
					UpdateSpeed();
					break;
				case speedIndex:
					UpdateDistance();
					UpdateTime();
					break;
			}
		}

		public override string ToString() => $"Time : {Time}, Distance : {Distance}, Speed : {Speed}";
	}
}