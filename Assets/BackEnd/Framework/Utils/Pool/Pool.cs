﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Framework.Utils
{

    /// <summary>
    /// Cette classe gère la récupération en vue d'éviter leur desctruction de chaque cube possèdés pars les ennemies en fin de vie.
    /// Elle dispache ainsi les cubes récupérés en fonction de la demande et dans le cas ou il n'y en a aucun en stock les instancies. 
    /// </summary>
    /// @Author Q.Mandou
    [Serializable]
    public class Pool<T> : MonoBehaviour where T : MonoBehaviour
    {
        public GameObject prefabs;

        [Tooltip("If null pool use own transform")]
        public Transform container = null;

        Queue<T> pools;

        /// <summary>
        /// Ajour d'élement dans la Queue de la pool.
        /// </summary>
        /// <param name="_object">Objet à ajouter</param>
        public virtual void Add(T _object)
        {
            _object.transform.SetParent(container);
            _object.transform.localPosition = container.position;
            _object.gameObject.SetActive(false);
            pools.Enqueue(_object);
        }

        /// <summary>
        /// Récupération du premier objet de la queue. LIFO (last in first out)
        /// </summary>
        /// <returns></returns>
        public virtual T Get()
        {
            pools = pools ?? new Queue<T>();

            if (pools.Count > 0)
            {
                T cube = pools.Dequeue();
                cube.gameObject.SetActive(true);
                return cube;
            }

            Transform usedTransform = container ?? transform;

            GameObject g = Instantiate(prefabs, usedTransform.position, Quaternion.identity, usedTransform);
            return g.GetComponent<T>();
        }
    }

}