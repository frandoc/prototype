﻿
namespace Framework.MakeFile
{
	/// <summary>
	/// Utiliser pour synthétiser l'écriture de fichier. 
	/// </summary>
	/// @Author Q.Mandou

	public class Writter
	{
		public string text;

		int tabCount;

		public Writter() => Reset();

		public void Reset()
		{
			text = "";
			tabCount = 0;
		}

		public void AddLine(string line = "") => text += $"{GetTabs(tabCount)}{line}\n";

		void Hook(bool open) => AddLine(open ? "{" : "}");

		public void OpenHook() => Hook(true);
		public void CloseHook() => Hook(false);

		public void UpTab() => tabCount++;

		public void DownTab() => tabCount--;

		string GetTabs(int tabCount)
		{
			string text = "";
			for (int i = 0; i < tabCount; i++)
			{
				text += "\t";
			}
			return text;
		}
	}
}