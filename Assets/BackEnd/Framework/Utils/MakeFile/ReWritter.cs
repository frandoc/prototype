﻿
using File = System.IO.File;

namespace Framework.MakeFile
{
	/// <summary>
	/// Utiliser pour réécrire dans un fichier existant.
	/// L'initialisation se fais à partir du chemin de dossier donné.
	/// </summary>
	/// @Author Q.Mandou

	public class ReWritter
	{
		string path;

		string[] lines;

		int linesCount => lines.Length;

		bool isDirty = false;

		public bool IsDirty { get => isDirty; }

		public bool IsOutOfRange(int index) => linesCount > index;

		public ReWritter(string _path)
		{
			string text = File.ReadAllText(_path);
			path = _path;
			lines = text.Split('\n');
		}

		public void ModifyLine(int id, string line)
		{
			if (IsOutOfRange(id))
			{
				lines[id] = line;
				isDirty = true;
			}
		}

		public void ModifyLine(string toModify, string line)
		{
			for (int i = 0; i < linesCount; i++)
			{
				if (lines[i] == toModify)
				{
					lines[i] = line;
				}
			}
		}

		public void AddToLine(int id, string toAdd)
		{
			if (IsOutOfRange(id))
			{
				lines[id] += toAdd;
				isDirty = true;
			}
		}

		public void Save()
		{
			string text = "";
			foreach (string s in lines)
			{
				text += s + '\n';
			}
			File.WriteAllText(path, text);
			isDirty = false;
		}
	}
}
