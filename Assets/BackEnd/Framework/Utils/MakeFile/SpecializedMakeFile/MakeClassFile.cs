﻿
using System.Collections.Generic;

namespace Framework.MakeFile
{
	/// <summary>
	/// Hérité de MakeFile utilisé pour la génération de code.
	/// </summary>
	/// @Author Q.Mandou

	public class MakeClassFile : MakeFile
	{
		public static void WriteCsFile( EPrivacy _classPrivacy,
										bool _classIsStatic,
										string _className,
										string _namespace,
										string _path,
										string _fileName = null,
										List<string> usings = null,
										List<string> commentaries = null,
										List<Variable> _variables = null,
										List<Property> _properties = null,
										List<Function> _functions = null)
		{
			if ((_fileName != null && _fileName.Length <= 0) || _className.Length <= 0)
			{
				UnityEngine.Debug.LogError("Filename ou classname ne doivent pas être vide. Le fichier n'a pas était généré.");
			}
			else
			{
				MakeClassFile maker = new MakeClassFile();
				maker.WriteUsings(usings);
				maker.OpenNameSpace(_namespace);
				maker.Space();
				maker.WriteWarning();
				maker.Space();
				maker.WriteCommentaries(commentaries);
				maker.OpenClass(_classPrivacy, _classIsStatic, _className);
				maker.WriteVariables(_variables);
				maker.WriteProperties(_properties);
				maker.WriteFunctions(_functions);
				maker.CloseClass();
				maker.CloseNameSpace();
				maker.Write(_path, _fileName);
			}
		}
	}
}