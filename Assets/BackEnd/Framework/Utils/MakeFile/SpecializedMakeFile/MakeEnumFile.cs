﻿
using System.Collections.Generic;

namespace Framework.MakeFile
{
	/// <summary>
	/// Hérité de MakeFile utilisé pour la génération d'enum.
	/// </summary>
	/// @Author Q.Mandou

	public class MakeEnumFile : MakeFile
	{
		public static void WriteEnumFile(string _namespace, string _enumName, EPrivacy _enumPrivacy, List<string> _enumNames, string _commentaries, string _path, string _fileName = null)
		{
			WriteEnumFile(_namespace, _enumName, _enumPrivacy, _enumNames, _commentaries != null ? new List<string>() { _commentaries } : new List<string>(), _path, _fileName);
		}

		public static void WriteEnumFile(string _namespace, string _enumName, EPrivacy _enumPrivacy, List<string> _enumNames, List<string> _commentaries, string _path, string _fileName = null)
		{
			if ((_fileName != null && _fileName.Length <= 0) || _enumName.Length <= 0)
			{
				UnityEngine.Debug.LogError("_fileName ou _enumName ne doivent pas être vide. Le fichier n'a pas était généré.");
			}
			else
			{
				MakeEnumFile maker = new MakeEnumFile();
				maker.SetEnumtext(_namespace, _enumName, _enumPrivacy, _enumNames, _commentaries);
				maker.Write(_path, _fileName);
				maker.Reset();
			}
		}

		void SetEnumtext(string _namespace, string _enumName, EPrivacy _enumPrivacy, List<string> _enumNames, List<string> _commentaries)
		{
			textFile.AddLine();
			OpenNameSpace(_namespace);
			WriteWarning();
			Space();
			WriteCommentaries(_commentaries);
			Space();
			WriteEnum(_enumPrivacy, _enumName, _enumNames);
			CloseNameSpace();
		}
	}
}