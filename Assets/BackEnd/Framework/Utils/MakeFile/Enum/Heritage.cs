﻿
using System;

namespace Framework.MakeFile
{
	public enum EHeritage
	{
		eOverride,
		eVirtual,
		eAbstract,
		eEmpty
	}

	/// <summary>
	/// Utilisé pour set l'héritage d'une classe d'un fichier géneré.
	/// </summary>
	/// @Author Q.Mandou

	public static class Heritage
	{
		static string[] heritageNames = { "override", "virtual", "abstract", "" };

		public static string GetName(EHeritage heritage) => heritageNames[Convert.ToInt32(heritage)];
	}

}