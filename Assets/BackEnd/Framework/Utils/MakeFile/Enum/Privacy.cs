﻿
namespace Framework.MakeFile
{
	public enum EPrivacy
	{
		ePrivate,
		eProtected,
		ePublic,
		eEmpty
	}

	/// <summary>
	/// Utilisé pour set le niveau de protection d'un fichier géneré.
	/// </summary>
	/// @Author Q.Mandou

	public static class Privacy
	{
		static string[] privacyNames = { "private", "protected", "public", "" };

		public static string GetString(EPrivacy privacy) => privacyNames[System.Convert.ToInt32(privacy)];
	}

}