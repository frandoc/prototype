﻿
using Framework.MakeFile;
using System.Collections.Generic;
using System.IO;

namespace Framework.MakeFile
{
	/// <summary>
	/// Code générique "Subclass pattern", base de tous les différents modes d'écriture utilisé.
	/// </summary>
	/// @Author Q.Mandou

	public abstract class MakeFile
	{
		protected Writter textFile;

		const string warningMessage = "// !!! This file is auto-generated don't modify this manualy if you don't want to corrupt. !!!";

		protected string IsStatic(bool isStatic) => isStatic ? "static" : "";

		public MakeFile() => textFile = new Writter();

		protected void Write(string path, string fileName = null, string extention = "cs")
		{
			if (fileName != null)
			{
				File.WriteAllText($"{Path.Combine(path, fileName)}.{extention}", textFile.text);
			}
			else
			{
				File.WriteAllText(path, textFile.text);
			}
		}

		protected void WriteUsings(List<string> usings)
		{
			foreach (string u in usings)
			{
				textFile.AddLine($"using {u};");
			}
		}

		protected void WriteEnum(EPrivacy privacy, string className, List<string> enumNames)
		{
			int count = 0;
			textFile.AddLine($"{Privacy.GetString(privacy)} enum {className}");
			textFile.OpenHook();
			textFile.UpTab();
			foreach (string name in enumNames)
			{
				string sufix = count < enumNames.Count - 1 ? "," : "";
				textFile.AddLine($"{name}{sufix}");
				count++;
			}
			textFile.DownTab();
			textFile.CloseHook();
		}

		protected void WriteWarning() => textFile.AddLine(warningMessage);

		protected void WriteFunctions(List<Function> functions)
		{
			foreach(Function function in functions)
			{
				textFile.AddLine();
				function.Write(textFile);
				textFile.AddLine();
			}
		}

		protected void WriteCommentaries(List<string> commentaries)
		{
			if (commentaries == null)
			{
				return;
			}

			foreach (string commentary in commentaries)
			{
				textFile.AddLine($"// {commentary}");
			}
		}

		protected void WriteVariables(List<Variable> variables)
		{
			if (variables == null)
			{
				return;
			}

			textFile.AddLine();
			foreach (Variable variable in variables)
			{
				variable.WriteVariable(textFile);
			}
		}

		protected void WriteProperties(List<Property> properties)
		{
			if (properties == null)
			{
				return;
			}

			textFile.AddLine();
			foreach (Property property in properties)
			{
				property.WriteProperty(textFile, EPropertyWriteMode.inline);
			}
		}

		protected void OpenNameSpace(string namespaceName)
		{
			textFile.AddLine($"namespace {namespaceName}");
			textFile.OpenHook();
			textFile.UpTab();
		}

		protected void CloseNameSpace()
		{
			textFile.DownTab();
			textFile.CloseHook();
		}

		protected void OpenClass(EPrivacy privacy, bool isStatic, string className, string parentClassName = null, List<string> interfaces = null)
		{
			string GetParentClassText() => parentClassName != null ? $": {parentClassName}" : "";

			string GetInterfaceText()
			{
				if (interfaces == null)
					return "";

				string toReturn = "";

				if(parentClassName == null)
				{
					toReturn = $" : {interfaces[0]}";
				}

				for(int i = parentClassName == null ? 1 : 0; i < interfaces.Count - 1; i++)
				{
					toReturn += $", {interfaces[i]}"; 
				}
				return toReturn;
			}

			string space = isStatic ? " " : "";

			textFile.AddLine();
			textFile.AddLine($"{Privacy.GetString(privacy)}{space}{IsStatic(isStatic)} class {className} {GetParentClassText()} {GetInterfaceText()}");
			textFile.OpenHook();
			textFile.UpTab();
		}

		protected void CloseClass()
		{
			textFile.DownTab();
			textFile.CloseHook();
		}

		protected void Space() => textFile.AddLine();

		public void Reset() => textFile.Reset();
	}

}
