﻿
using System.Collections.Generic;

namespace Framework.MakeFile
{
	/// <summary>
	/// Contient toutes les informations nécessaire à l'écriture d'une property.
	/// </summary>
	/// @Author : Q.Mandou

	public enum EPropertyWriteMode
	{
		inline,
		getSetInline,
		complete
	}

	public struct Property
	{
		// Const
		const string property = "{ get; set; }";

		// Var
		EPrivacy privacy;
		bool isStatic;
		string type;
		string propertyName;
		string initValue;
		List<string> getInstruction;
		List<string> setInstruction;

		string IsStatic => isStatic ? "static" : "";
		string Space => isStatic ? " " : "";

		public Property(EPrivacy _privacy, bool _isStatic, string _type, string _propertyName, string _initValue, List<string> _getInstruction = null, List<string> _setInstruction = null)
		{
			privacy = _privacy;
			isStatic = _isStatic;
			type = _type;
			propertyName = _propertyName;
			initValue = _initValue;
			getInstruction = _getInstruction;
			setInstruction = _setInstruction;
		}

		string GetInitValue() => initValue == null ? ";" : $"= new {type}({initValue});";

		/// <summary>
		/// Inline : Proprety sur une ligne en lambda
		/// PropsInline : Getter & setter à une seule instruction en lambda. Les list doivent être non null, seulement le premier élément est utilisé.
		/// Complete : Getter & setter avec plusieurs instruction Les list doivent être non null.
		/// </summary>
		public void WriteProperty(Writter textFile, EPropertyWriteMode writeMode)
		{
			switch (writeMode)
			{
				case EPropertyWriteMode.inline:
					WriteInlineProperty(textFile);
					break;
				case EPropertyWriteMode.getSetInline:
					WriteGetSet(textFile, true);
					break;
				case EPropertyWriteMode.complete:
					WriteGetSet(textFile, false);
					break;
			}
		}

		void WriteInlineProperty(Writter textFile) => textFile.AddLine($"{Privacy.GetString(privacy)}{Space}{IsStatic} {type} {propertyName} {property} {GetInitValue()}");

		void WriteGetSet(Writter textFile, bool inline)
		{
			textFile.AddLine($"{Privacy.GetString(privacy)}{Space}{IsStatic} {type} {propertyName}");
			textFile.OpenHook();
			textFile.UpTab();
			if (inline)
			{
				WriteInlineGetSet(textFile, true);
				WriteInlineGetSet(textFile, false);
			}
			else
			{
				WriteCompleteGetSet(textFile, true);
				WriteCompleteGetSet(textFile, false);
			}
			textFile.DownTab();
			textFile.CloseHook();
		}

		void WriteInlineGetSet(Writter textFile, bool get)
		{
			string GetOrSet() => get ? "get" : "set";
			string instruction = get ? getInstruction[0] : setInstruction[0];
			textFile.AddLine($"{GetOrSet()} => {instruction};");
		}

		void WriteCompleteGetSet(Writter textFile, bool get)
		{
			textFile.AddLine(get ? "get" : "set");
			textFile.OpenHook();
			{
				textFile.UpTab();
				foreach (string instruction in get ? getInstruction : setInstruction)
				{
					textFile.AddLine(instruction);
				}
				textFile.DownTab();
			}
			textFile.CloseHook();
		}
	}
}