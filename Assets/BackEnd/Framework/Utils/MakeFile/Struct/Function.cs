﻿
using System.Collections.Generic;
using System.Diagnostics;

namespace Framework.MakeFile
{
	/// <summary>
	/// Structure de donnée autonome nécessaire à l'écriture d'une fonction.
	/// Attention aux fonctions qui doivent retourner qqc en fonction de si elle est inline ou non.
	/// La fonction ne serat pas écrite si elle ne dispose pas d'au moin une instruction.
	/// Elle sera inline si elle possède une seule instruction,
	/// sinon elle sera sur plusieurs lignes.
	/// </summary>
	/// @Author : Q.Mandou

	public struct Function
	{
		EPrivacy privacy;
		bool isStatic;
		EHeritage heritage;
		string returnType;
		string name;
		List<string> arguments;
		List<string> instructions;

		string IsStatic => isStatic ? "static" : "";

		public Function(EPrivacy _privacy, bool _isStatic, EHeritage _heritage, string _returnType, string _name, List<string> _instructions, List<string> _arguments = null)
		{
			privacy = _privacy;
			isStatic = _isStatic;
			heritage = _heritage;
			returnType = _returnType;
			name = _name;
			arguments = _arguments;
			instructions = _instructions;
		}

		public void Write(Writter textFile)
		{
			if (instructions.Count > 1)
			{
				WriteNormal(textFile);
			}
			else if(instructions.Count > 0)
			{
				WriteInline(textFile);
			}
			else
			{
				UnityEngine.Debug.LogError("La liste d'instructions pour écrire une fonction, nécessite au moins une instruction.");
			}
		}

		void WriteNormal(Writter textFile)
		{
			string spaceA = isStatic ? " " : "";
			string spaceB = heritage != EHeritage.eEmpty ? " " : "";
			textFile.AddLine($"{Privacy.GetString(privacy)} {IsStatic}{spaceA}{Heritage.GetName(heritage)}{spaceB}{returnType} {name}{GetArgumentsLine()}");
			textFile.OpenHook();
			textFile.UpTab();
			foreach (string instruction in instructions)
			{
				textFile.AddLine(instruction);
			}
			textFile.DownTab();
			textFile.CloseHook();
		}

		void WriteInline(Writter textFile)
		{
			string spaceA = isStatic ? " " : "";
			string spaceB = heritage != EHeritage.eEmpty ? " " : "";
			textFile.AddLine($"{Privacy.GetString(privacy)} {IsStatic}{spaceA}{Heritage.GetName(heritage)}{spaceB}{returnType} {name}{GetArgumentsLine()} {GetInlineInstructions()}");
		}

		string GetArgumentsLine()
		{
			if (arguments == null)
				return "()";

			string toReturn = "(";
			for (int i = 00; i < arguments.Count - 1; i++)
			{
				toReturn += $"{arguments[i]}, ";
			}
			return $"{toReturn}{arguments[arguments.Count - 1]})";
		}

		string GetInlineInstructions() => $" => {instructions[0]}";
	}

}