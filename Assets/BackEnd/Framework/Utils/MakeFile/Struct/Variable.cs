﻿
namespace Framework.MakeFile
{
	/// <summary>
	/// Contient toutes les informations nécessaire à l'écriture d'une variable.
	/// </summary>
	/// @Author : Q.Mandou
	
	public struct Variable
	{
		EPrivacy privacy;
		string name;
		string typeName;
		string initValue;

		public Variable(EPrivacy _privacy, string _typeName, string _name, string _initValue = null)
		{
			privacy = _privacy;
			typeName = _typeName;
			name = _name;
			initValue = _initValue;
		}

		public string GetInitValue() => initValue != null ? $" = {initValue};" : ";";

		public void WriteVariable(Writter writter) => writter.AddLine($"{Privacy.GetString(privacy)} {typeName} {name}{GetInitValue()}");

	}

}