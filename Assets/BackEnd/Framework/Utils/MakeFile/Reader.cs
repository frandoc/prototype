﻿
using System.Collections.Generic;

namespace Framework.MakeFile
{
	public static class Reader
	{
		/// <summary>
		/// Utilisé pour parser les infos utiles dans différents type de fichier.
		/// Doit être générisé dans un futur proche.
		/// </summary>
		/// @Author Q.Mandou

		public static List<string> ParseEnumText(string textFile)
		{
			List<string> toReturn = new List<string>();
			bool startRead = false;
			string[] lines = textFile.Split('\n');

			foreach (string s in lines)
			{
				if (s.LastIndexOf('\t') >= 0)
				{
					string parsed = s.Substring(s.LastIndexOf('\t') + 1);
					if (!startRead)
					{
						if (parsed.Length > 0 && parsed[0] == '{')
						{
							startRead = true;
						}
					}
					else
					{
						if (parsed.IndexOf(',') >= 0)
						{
							toReturn.Add(parsed.Substring(0, parsed.Length - 1));
						}
						else
						{
							toReturn.Add(parsed);
							return toReturn;
						}
					}
				}
			}
			return toReturn;
		}

		public static List<string> ParseABTestingGroupName(string textFile)
		{
			List<string> toReturn = new List<string>();
			string[] lines = textFile.Split('\n');

			foreach (string s in lines)
			{
				if (s.LastIndexOf('\t') >= 0 && s.Contains("// #"))
				{
					toReturn.Add(s.Substring(s.LastIndexOf('#') + 1));
				}
			}
			return toReturn;
		}

		const char strEnd = '\r';

		const string defineFalse = "//#define";
		const string defineTrue = "#define";

		const int defineFalseLenght = 9;
		const int defineTrueLenght = 7;

		public static Dictionary<string, bool> ParseDefine(string textFile)
		{
			Dictionary<string, bool> toReturn = new Dictionary<string, bool>();

			string[] lines = textFile.Split('\n');
			foreach (string s in lines)
			{
				int length = s.Length;

				if(s[0] == strEnd)
				{
					return toReturn;
				}

				if(length > defineFalseLenght)
				{
					if (s.Substring(0, defineFalseLenght) == defineFalse)
					{
						toReturn.Add(s.Substring(defineFalseLenght + 1, s.Length - defineFalseLenght - 2), false);
						continue;
					}
				}

				if (length > defineTrueLenght)
				{
					if (s.Substring(0, defineTrueLenght) == defineTrue)
					{
						toReturn.Add(s.Substring(defineTrueLenght + 1 , s.Length - defineTrueLenght - 2), true);
					}
				}
			}
			return toReturn;
		}
	}
}