﻿using System.IO;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Framework.Utils
{
	public static class IOUtils
	{
		public static string PathCombine(string a, string b)
		{
			return $"{a}/{b}";
		}

		/// <summary>
		/// Copie un dossier source ainsi que tous ces sous-dossiers vers un emplaçement cible.
		/// </summary>
		/// <param name="source">Dossier à copier</param>
		/// <param name="target">Dossier copié</param>
		public static void CopyAllFolder(DirectoryInfo source, DirectoryInfo target)
		{
			CreateDirectoryIfNotExist(target.FullName);

			foreach (FileInfo fileInfo in source.GetFiles())
			{
				fileInfo.CopyTo(Path.Combine(target.FullName, fileInfo.Name), true);
			}

			foreach (DirectoryInfo diSourceSubDir in source.GetDirectories())
			{
				DirectoryInfo nextTargetSubDir = target.CreateSubdirectory(diSourceSubDir.Name);
				CopyAllFolder(diSourceSubDir, nextTargetSubDir);
			}
		}

		/// <summary>
		/// Vérifie si un dossier existe et dans le cas contraire le crée.
		/// </summary>
		public static void CreateDirectoryIfNotExist(string path)
		{
			if (!Directory.Exists(path))
			{
				Directory.CreateDirectory(path);
			}
		}

		/// <summary>
		/// Vérifie si un fichier éxiste et dans le cas contraire le crée.
		/// </summary>
		/// <param name="path"></param>
		public static void CreateFileIfNotExist(string path)
		{
			if (!File.Exists(path))
			{
				File.Create(path).Dispose();
			}
		}

		public static void WriteAllBytes(string path, byte[] data)
		{
			CreateFileIfNotExist(path);
			File.WriteAllBytes(path, data);
		}

		public static void WriteAllText(string path, string text)
		{
			CreateFileIfNotExist(path);
			File.WriteAllText(path, text);
		}

		public static byte[] ReadAllByte(string path) => File.ReadAllBytes(path);


		public static void DeleteAllFiles(DirectoryInfo dirInfo)
		{
			foreach (FileInfo fileInfo in dirInfo.GetFiles())
			{
				fileInfo.Delete();
			}
		}

		/// <summary>
		/// Détruit un dossier et pour ce faire tous les fichiers qu'il contient
		/// ainsi que récursivement tous c'est sous dossier.
		/// </summary>
		/// <param name="dirInfo"></param>
		public static void DeleteRecursivelyDirectory(DirectoryInfo dirInfo)
		{
			// On regarde récursivant les sous dossiers
			foreach (DirectoryInfo insideDirInfo in dirInfo.GetDirectories())
			{
				DeleteRecursivelyDirectory(insideDirInfo);
			}
			// Quand il n'y en a plus on vide le dossier de ces fichiers 
			DeleteAllFiles(dirInfo);
			// Puis on le delete
			dirInfo.Delete();
		}

		/// <summary>
		/// Permet de nettoyer un chemin d'accés pour qu'il commence à partir du start donné
		/// ou juste après celui-ci.
		/// </summary>
		/// <param name="pathToClean">Chemin à néttoyer</param>
		/// <param name="pathStart">Le sous dossier à partir du quel on veut que le chemin nettoyer commence</param>
		/// <param name="includePathStart">Vrais si le start donné est inclu dans le chemin retourné</param>
		/// <returns></returns>
		public static string CleanPath(string pathToClean, string pathStart, bool includePathStart)
		{
			int index = pathToClean.IndexOf(pathStart) + (includePathStart ? 0 : pathStart.Length);
			return index > -1 ? pathToClean.Substring(index) : pathToClean;
		}

		/// <summary>
		/// Récupère depuis un dossier et récursivement dans ces 
		/// sous-dossiers tous les chemins de fichier ayant la 
		/// même extension et les concaténe dans un string séparé 
		/// d'un retour chariot '\n'
		/// </summary>
		/// <param name="dirInfo">Dossier à analyser</param>
		/// <param name="extension"></param>
		/// <param name="pathStart"></param>
		/// <returns></returns>
		public static List<string> GetAllFilePathWithSameExtension(DirectoryInfo dirInfo, string extension, string pathStart)
		{
			List<string> toReturn = new List<string>();
			foreach (FileInfo fileInfo in dirInfo.GetFiles())
			{
				if (fileInfo.Extension == extension)
				{
					toReturn.Add(pathStart != null ? CleanPath(fileInfo.FullName, pathStart, false) : fileInfo.FullName);
				}
			}

			foreach (DirectoryInfo dir in dirInfo.GetDirectories())
			{
				toReturn.AddRange(GetAllFilePathWithSameExtension(dir, extension, pathStart));
			}

			return toReturn;
		}

		public static void ConstructPathHierarchy(string path)
		{
			if(File.Exists(path))
			{
				return;
			}

			string[] paths = path.Split('/');
			string pathToCreate = paths[0];

			for (int i = 1; i < paths.Length - 1; i++)
			{
				pathToCreate = Path.Combine(pathToCreate, paths[i]);
				CreateDirectoryIfNotExist(pathToCreate);
			}

			pathToCreate = Path.Combine(pathToCreate, paths[paths.Length - 1]);
			CreateFileIfNotExist(pathToCreate);
		}

		/// <summary>
		/// Méthode d'extention pour concaténer une list de string en une seule string.
		/// </summary>
		/// <param name="list"></param>
		/// <param name="separator"></param>
		/// <returns></returns>
		public static string Concatenate(this List<string> list, char separator = '\n')
		{
			string toReturn = "";
			foreach (string text in list)
			{
				toReturn += text + separator;
			}
			return toReturn;
		}

		public static IEnumerable<string> GetAllFilePathWithSameExtensionList(DirectoryInfo directoryInfo, string v1, string v2)
		{
			throw new NotImplementedException();
		}
	}
}