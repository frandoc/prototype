﻿using Framework.Utils.DesignPattern;
using UnityEngine;

namespace Games.Data
{
	public abstract class Data : MonoBehaviour
	{
		public string filePath;

		public virtual void Load()
		{
			throw new System.NotImplementedException();
		}

		public virtual void Save()
		{
			throw new System.NotImplementedException();
		}
	}

}
