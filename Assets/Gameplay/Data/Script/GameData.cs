﻿using UnityEngine;
using Games.PoolWrapper;

namespace Games.Data
{
	public class GameData : Data
	{
		[SerializeField] GameObject player = null;

		[SerializeField] PoolWrappers poolWrapper = null;

		public PoolWrappers PoolWrapper => poolWrapper;

		public Transform PlayerTransform
		{
			get => player.transform;
		}
	}
}
