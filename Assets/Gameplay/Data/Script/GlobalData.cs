﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Framework.Utils.DesignPattern;
using System;
using System.Data;

namespace Games.Data
{
    public class GlobalData : Singleton<GlobalData>
    {
        public GameObject dataContainer;

        ConstantData constantData = null;
        public Dictionary<Type, Data> datas = null;

        public static GameData gameData => Instance.datas[typeof(GameData)] as GameData;

        public static UIData userInterfaceData => Instance.datas[typeof(UIData)] as UIData;

        protected new void Awake()
		{
            base.Awake();
            InitData();
        }

		public void InitData()
		{
            constantData = new ConstantData();
            datas = new Dictionary<Type, Data>();
            Data[] dataArray = dataContainer.GetComponents<Data>();
            foreach(Data data in dataArray)
            {
                datas[data.GetType()] = data;
            }
        }

        public void AllSaveData()
		{
            foreach(Type type in datas.Keys)
			{
                datas[type].Save();
            }
		}

        public void AllLoadData()
        {
            foreach (Type type in datas.Keys)
            {
                datas[type].Load();
            }
        }
    }
}
