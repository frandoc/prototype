﻿using UnityEngine;

using Games.AttackComponents;

namespace Controller
{
    public class Controller : MonoBehaviour
    {
        public Transform player;
        public float speed = 5.0f;
        private bool touchStart = false;
        private Vector2 startOffset;
        private Vector2 endOffset;

        public Transform circle;
        public Transform outerCircle;

        public AttackComponent playerAttackComponent;

        // Update is called once per frame
        void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                startOffset = Camera.main.ScreenToViewportPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.transform.position.z));

                circle.transform.position = startOffset * -1;
                outerCircle.transform.position = startOffset * -1;
                circle.gameObject.SetActive(true);
                outerCircle.gameObject.SetActive(true);
            }
            if (Input.GetMouseButton(0))
            {
                touchStart = true;
                endOffset = Camera.main.ScreenToViewportPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.transform.position.z));
            }
            else
            {
                touchStart = false;
            }

            AttackXboxController();
        }
        private void FixedUpdate()
        {
            ///Xbox Controller
            moveCharacterJoystick(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
            rotateCharacterJoystick();

            ///Mobile
            if (touchStart)
            {
                Vector2 offset = endOffset - startOffset;
                Vector2 direction = Vector2.ClampMagnitude(offset, 1.0f);
                moveCharacter(direction * 1);

                circle.transform.position = new Vector2(startOffset.x + direction.x, startOffset.y + direction.y) * 5;
            }
            else
            {
                circle.gameObject.SetActive(false);
                outerCircle.gameObject.SetActive(false);
            }

        }
        void moveCharacter(Vector2 direction)
        {
            player.Translate(direction.x * speed * Time.deltaTime, 0, direction.y * speed * Time.deltaTime);
        }
        void moveCharacterJoystick(float _x, float _y)
        {
            player.Translate(_x * speed * Time.deltaTime, 0, _y * speed * Time.deltaTime, Space.World);
        }

        void rotateCharacterJoystick()
        {
            float heading = Mathf.Atan2(Input.GetAxis("RightXAxis"), Input.GetAxis("RightYAxis"));
            Quaternion eulerRot = Quaternion.Euler(0.0f, heading * Mathf.Rad2Deg, 0.0f);
            player.transform.rotation = Quaternion.Lerp(transform.rotation, eulerRot, Time.deltaTime * 45.0f);
        }

        void AttackXboxController()
        {
            if (Input.GetButton("Fire1") || Input.GetAxis("Fire1") >= 0.5f)
            {
                playerAttackComponent.Attack();
            }
        }
    }
}
