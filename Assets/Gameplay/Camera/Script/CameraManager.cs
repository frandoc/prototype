﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    public Transform target;

    public float distance = 7.0f;
    public float angleX = 35.0f;
    public float angleY = 0.0f;
    public float angleZ = 0.0f;
    public float height = 10.0f;
    public float speed = 1.0f;

    private Vector3 velocity;

    // Start is called before the first frame update
    void Start()
    {
        CameUpdate();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //#TODO => Here Do Delegtae => if need to change do a change, scape multiple fram's
        CameUpdate();
    }

    public virtual void CameUpdate()
    {
        if (!target)
        {
            return;
        }
        Vector3 Pos = new Vector3();
        Pos.x = target.position.x;
        Pos.y = target.position.y + height;
        Pos.z = target.position.z - distance;
        transform.position = Vector3.SmoothDamp(transform.position, Pos, ref velocity, speed);

        transform.rotation = Quaternion.Euler(angleX, angleY, angleZ);
    }
}
