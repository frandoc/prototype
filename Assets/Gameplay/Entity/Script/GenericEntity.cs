﻿
using UnityEngine;
using UnityEngine.AI;
using Games.AttackComponents;


namespace Games.Entity
{

    public abstract class GenericEntity : MonoBehaviour
    {
        public float behaviourDelay = 0.05f;
        public AttackComponent attackComponent = null;

        Transform target = null;
        float timer = 0.0f;

        //Must be initialise in inspector
        [SerializeField] NavMeshAgent agent = null;

        public virtual void Init() { }

        public void UpdateTarget()
		{
            if(behaviourDelay >= timer)
			{
                Target(target);
                timer = 0.0f;
			}
            else
			{
                timer += Time.deltaTime;
			}
        }

		public void Target(Transform _transform = null)
        {
            target = _transform;
            if (target == null)
            {
                Stop();
            }
            else
            {
                MoveToTarget();
            }
        }

        public bool MoveToTarget() => agent.SetDestination(target.position);

        public void Stop() => agent.isStopped = true;

    }

}