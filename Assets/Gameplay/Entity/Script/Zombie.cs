﻿using Games.Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Games.Entity
{
    public class Zombie : GenericEntity
    {
        Transform playerTarget => GlobalData.gameData.PlayerTransform;

        public override void Init() => TargetPlayer();

        // Update is called once per frame
        void Update()
        {
            UpdateTarget();
        }


        public void TargetPlayer() => Target(playerTarget);
    }

}