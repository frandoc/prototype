﻿using Framework.Utils;
using Games.Entity;

namespace Games.PoolWrapper
{
	public class GenericEntityPool : Pool<GenericEntity>, IPool { }
}
