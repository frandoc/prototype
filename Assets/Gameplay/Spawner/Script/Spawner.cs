﻿
using Games.PoolWrapper;
using UnityEngine;
using Framework.Events;
using Framework.Utils.DesignPattern;
using Games.Entity;

public class Spawner : MonoBehaviour
{
    public int startSpawnner;

    public PoolWrappers poolwrapper;

    [SerializeField] GenericEntityPool genericEntityPool;

    void Spawn()
	{
        GenericEntity ge = genericEntityPool.Get();
        if(ge != null)
		{
            ge.transform.position = transform.position;
            ge.Init();
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        genericEntityPool = poolwrapper.GetPool<GenericEntityPool>() as GenericEntityPool;
        SpawnWithDelay(10, 2.0f);
    }

    public void SpawnWithDelay(int spawnCount, float delayBetweenTwoSpawn)
	{
        float sumDelay = 0.0f;
        for(int i = 0; i < spawnCount; i++)
		{
            sumDelay += delayBetweenTwoSpawn;
            this.SubscribeTimedEvent(sumDelay, () => Spawn());
        }
	}
}
