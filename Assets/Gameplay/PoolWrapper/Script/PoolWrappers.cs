﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Games.PoolWrapper
{
	public interface IPool { } 

	public class PoolWrappers : MonoBehaviour
	{
		Dictionary<Type, IPool> container = null;

		void Awake()
		{
			container = new Dictionary<Type, IPool>();
			foreach (IPool pool in GetComponentsInChildren<IPool>())
			{
				container[pool.GetType()] = pool;
			}
		}

		public IPool GetPool<T>() => container[typeof(T)];

	}

}
