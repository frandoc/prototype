﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// Use for make difference with other weapon collider
/// </summary>
public interface IPlayer 
{
	void ApplyDamage(int _damage);
}
