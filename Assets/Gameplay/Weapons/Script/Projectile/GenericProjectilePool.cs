﻿
using Framework.Utils;
using Games.PoolWrapper;

namespace Games.Weapons
{
    public class GenericProjectilePool : Pool<Bullet>, IPool { }
}
