﻿using Framework.Utils;
using UnityEngine;

namespace Games.Weapons
{
    public class Bullet : MonoBehaviour
    {
        public float velocity = 0.0f;
        public float lifetime = 0.0f;

        private Vector3 direction;
        private bool bNeedUpdate = false;

        private void Awake()
        {
            Destroy(gameObject, lifetime);
        }

        public void Update()
        {
            if (bNeedUpdate)
            {
                transform.position += (direction * velocity) * Time.deltaTime;
            }
        }

        public void InitProjectile(Transform _muzzleLocation,Vector3 _direction, float _velocity)
        {
            transform.position = _muzzleLocation.position;
            direction = _direction;
            velocity = _velocity;
            bNeedUpdate = true;
        }

        public void OnCollisionEnter(Collision collision)
        {

        }
        public void OnCollisionExit(Collision collision)
        {
            
        }
    }
}