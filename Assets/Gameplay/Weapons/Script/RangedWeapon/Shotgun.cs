﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Games.Weapons
{
    public class Shotgun : RangeWeapon
    {
        public int nbShells = 12;
        public float startAngleArea = 135.0f;
        public float endingAngleArea = 225.0f;

        public override void Attack()
        {

            if (!AllowFire())
                return;

            float offsetAngle = (endingAngleArea - startAngleArea) / nbShells;

            for (int i = 1; i <= nbShells; i++)
            {
                //float currentAngle = (offsetAngle * i) + startAngleArea;
                float currentAngle = (Random.Range(1.5f, offsetAngle) * i) + startAngleArea;
                Vector3 Angle = new Vector3(0, currentAngle, 0);
                Vector3 direction = Quaternion.Euler(Angle) * muzzleLocation.right;

                GetAndInitBullet(muzzleLocation, -direction);
            }
        }
    }
}