﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Games.Weapons
{
    public class Burst : RangeWeapon
    {
        public int nbBulletRafale = 4;

        public override void Attack()
        {

            if (!AllowFire())
                return;

            for (int i = 0; i <= nbBulletRafale; i++)
            {
                float currentAngle = (Random.Range(0f, spread) * i);
                Vector3 Angle = new Vector3(0, currentAngle, 0);
                Vector3 direction = Quaternion.Euler(Angle) * muzzleLocation.right;

                GetAndInitBullet(muzzleLocation, direction);
            }
        }
    }
}