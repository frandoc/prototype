﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Games.Weapons
{
    public class GenericWeapon : MonoBehaviour
    {
        /* Generic Weapon Settings */
        public int damage = 0;
        public int spread = 0;

        public float firerate = 0.0f;
        private float nextshot = 0.0f;
        public int ameliorationlevel = 0;
        public bool isInifinite = false;
        public float timerUsageLimitation = 0.0f;
        public bool isAkimbo = false;
        private bool allowFire = false;
        public AudioSource attackSound;
        public ParticleSystem attackFX;

        protected void Awake() { }

        protected void Start() { }

		public virtual void Attack() { }

        public virtual bool AllowFire()
        {
            if (Time.time > nextshot)
            {
                nextshot = Time.time + firerate;
                return allowFire = true;
            }
            else
            {
                return allowFire = false;
            }
        }
    }
}