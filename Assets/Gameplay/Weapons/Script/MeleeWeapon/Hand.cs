﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Games.Weapons
{
    public class Hand : MeleeWeapon
    {

		private void OnTriggerEnter(Collider other)
		{
			IPlayer player = other.GetComponent<IPlayer>();
			if (player != null)
			{
				Debug.Log("Player receive " + damage);
				player.ApplyDamage(damage);
			}
		}
	}
}
