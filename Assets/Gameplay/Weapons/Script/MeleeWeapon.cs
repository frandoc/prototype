﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Games.Weapons
{
    public enum MeleeType
    {
        ColliderHit,
        RaycastHit,
        AnimationHit,
    };

    public class MeleeWeapon : GenericWeapon
    {
        public int slashRange;

        public MeleeType meleeType;
        public override void Attack()
        {
            base.Attack();

            switch (meleeType)
            {
                case MeleeType.ColliderHit:
                    break;
                case MeleeType.RaycastHit:
                    break;
                case MeleeType.AnimationHit:
                    break;
                default:
                    break;
            }
        }
    }
}
