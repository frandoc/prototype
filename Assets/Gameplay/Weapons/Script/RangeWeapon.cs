﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Games.PoolWrapper;
using Games.Data;

namespace Games.Weapons
{
    public class RangeWeapon : GenericWeapon
    {
        public GenericProjectilePool poolBulletType;
        public Transform muzzleLocation;

		protected new void Start()
		{
            base.Start();
            poolBulletType = GlobalData.gameData.PoolWrapper.GetPool<GenericProjectilePool>() as GenericProjectilePool;
        }

		public override void Attack()
        {
            base.Attack();

            if (!AllowFire())
                return;

            GetAndInitBullet(muzzleLocation, muzzleLocation.right);
        }

        /// <summary>
        /// Use to get bullet from pool 
        /// </summary>
        /// <param name="_muzzleLocation">Transform of bullet out</param>
        /// <param name="_direction">set projectile direction</param>
        /// <param name="_velocity">set projectile velocity</param>
        protected void GetAndInitBullet(Transform _muzzleLocation, Vector3 _direction, float _velocity = 0.0f)
        {
            if (poolBulletType == null)
                return;

            Bullet bulletClone = poolBulletType.Get();
            bulletClone.InitProjectile(_muzzleLocation, _direction, _velocity != 0.0f ? _velocity : bulletClone.velocity);
        }
    }
}
