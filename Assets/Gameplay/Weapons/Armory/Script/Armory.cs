﻿
using System;
using System.Collections.Generic;
using UnityEngine;

using Framework.Utils.DesignPattern;
using Games.Weapons;

namespace Games.Armory
{
	public class Armory : Singleton<Armory>
	{
#pragma warning disable 0649

		[SerializeField] List<GenericWeapon> armory;

		Dictionary<Type, GenericWeapon> container = null;

#pragma warning restore 0649

		private void Awake()
		{
			if (container == null)
			{
				container = new Dictionary<Type, GenericWeapon>();
				foreach (GenericWeapon gw in armory)
				{
					container.Add(gw.GetType(), gw);
				}
			}
		}

		public T GetWeapon<T>() where T : GenericWeapon => container[typeof(T)] as T;
	}

}