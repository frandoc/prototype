﻿using System.Collections;
using UnityEngine;

namespace Games.Player
{
    public class Player : MonoBehaviour, IPlayer
    {
        /* Var */
        public int health;

        /* Engine */
        void Start()
        {

        }

        void Update()
        {

        }


        /* Custom */

        /// <summary>
        /// Surchage interface Player
        /// </summary>
        /// <param name="_damage"></param>
        public void ApplyDamage(int _damage)
        {
            health -= _damage;

            if (health < 0)
            {
                Death();
            }
        }

        public void RegenHealth(int _amount)
        {
            health += _amount;
        }

        public void RegenHealthOverTime(int _amount, float _second)
        {
            StartCoroutine(TimerHealthRegen(_amount, _second));
        }

        private IEnumerator TimerHealthRegen(int _amount, float _duration)
        {
            while (_amount >= 0)
            {
                health += 1;
                _amount -= 1;
                yield return new WaitForSeconds(1);
            }
            yield return null;
        }

        public void Death()
        {
            Debug.Log("You are dead !");
        }
    }

}