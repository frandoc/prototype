﻿
using UnityEngine;
using System.Collections.Generic;
using Games.Weapons;

namespace Games.AttackComponents
{
    public class AttackComponent : MonoBehaviour
    {
        //Weapons
        [SerializeField] Transform weaponSocketRight = null;
        [SerializeField] Transform weaponSocketLeft = null;

        [SerializeField] GenericWeapon defaultWeapon = null;

        GenericWeapon currentWeapon;
        Dictionary<string, GenericWeapon> Inventory = null;

        private float timerWeaponDuration = 0.0f;
        private bool bIsTimerfrozen = false;
        private float frozeduration = 0.0f;

        // Start is called before the first frame update
        void Start()
        {
            Inventory = new Dictionary<string, GenericWeapon>();
            InitDefaultWeapon();
        }

        //// Update is called once per frame
        //void Update()
        //{

        //}

        public void InitDefaultWeapon()
        {
            if (defaultWeapon == null)
                return;

            if (defaultWeapon.isAkimbo)
            {
                //#Todo Akimbo
                currentWeapon = Instantiate(defaultWeapon, weaponSocketRight);
                currentWeapon = Instantiate(defaultWeapon, weaponSocketLeft);
            }
            else
            {
                currentWeapon = Instantiate(defaultWeapon, weaponSocketRight);
            }
            Inventory.Add("Default", currentWeapon);
        }

        public void Attack()
        {
            currentWeapon.Attack();
        }

        void EquipWeapon(GenericWeapon _weapon)
        {
            if (_weapon != null)
            {
                currentWeapon.gameObject.SetActive(false);
                if (_weapon.isAkimbo)
                {
                    currentWeapon = Instantiate(defaultWeapon, weaponSocketRight);
                    currentWeapon = Instantiate(defaultWeapon, weaponSocketLeft);
                }
                else
                {
                    currentWeapon = Instantiate(_weapon, weaponSocketRight);
                }
                currentWeapon.gameObject.SetActive(true);
                Inventory.Add("PowerUp", currentWeapon);
            }
        }
    }
}
